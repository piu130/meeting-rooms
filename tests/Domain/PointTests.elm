module Domain.PointTests exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)
import Domain.Point exposing (decode)
import Domain.Decode as Decode


suite : Test
suite =
    describe "The Point module"
        [ describe "decode"
            [ test "valid" <|
                \_ ->
                  let
                    input = ""
                    result = Decode.decodeString decode input
                  in
                  Expect.equal (success result) True)
            ]
        ]
