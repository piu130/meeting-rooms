module Domain.Floor exposing (Field, Floor, Msg(..), Style, decoder, default, defaultStyle, empty, view, width)

import Color exposing (Color, toCssString)
import Domain.Point as Point exposing (Point)
import Json.Decode as Decode exposing (Decoder, decodeString, field, int, list, map, map2, string)
import Svg exposing (Svg)
import Svg.Attributes as SvgA
import Svg.Events as SvgE



-- MODEL


type Floor
    = Floor (List Point)


default : Floor
default =
    Result.withDefault empty <|
        decodeString decoder
            ("[ [1, 1, 1, 1, 0]"
                ++ ", [1, 1, 1, 1, 0]"
                ++ ", [1, 1, 0, 1, 1]"
                ++ ", [1, 1, 1, 1, 1]"
                ++ "]"
            )


type Field
    = None
    | Field Point


type Style
    = Style
        { fillColor : Color
        , borderColor : Color
        }


defaultStyle : Style
defaultStyle =
    Style
        { fillColor = Color.black
        , borderColor = Color.lightGrey
        }



-- VIEW


view : Int -> Style -> Floor -> Svg Msg
view fs s (Floor ps) =
    Svg.g
        [ SvgA.class "room-floor" ]
        (List.map (viewField fs s) ps)


viewField : Int -> Style -> Point -> Svg Msg
viewField fs (Style { borderColor, fillColor }) p =
    Svg.polygon
        [ SvgA.class "room-floor-field"
        , SvgA.id ("room-floor-field-" ++ Point.intersperse "-" p)
        , SvgA.stroke <| toCssString borderColor
        , SvgA.fill <| toCssString fillColor
        , viewFieldPoints fs p
        , SvgE.onClick <| FieldClicked p
        ]
        []


viewFieldPoints : Int -> Point -> Svg.Attribute a
viewFieldPoints fs { x, y } =
    let
        leftX =
            (y + x) * fs // 2

        leftY =
            (x - y) * fs // 4

        left =
            { x = leftX, y = leftY }

        top =
            { x = leftX + fs // 2, y = leftY - fs // 4 }

        right =
            { x = leftX + fs, y = leftY }

        bottom =
            { x = leftX + fs // 2, y = leftY + fs // 4 }
    in
    SvgA.points
        (Point.intersperse "," left
            ++ " "
            ++ Point.intersperse "," top
            ++ " "
            ++ Point.intersperse "," right
            ++ " "
            ++ Point.intersperse "," bottom
        )



-- UPDATE


type Msg
    = FieldClicked Point



-- SUBSCRIPTIONS


decoder : Decoder Floor
decoder =
    map Floor (Decode.map fromGrid (list (list int)))



-- styleDecoder : Decoder Style
-- styleDecoder =
--     map (Style) <| Decode.map2 (field "fillColor" string) (field "borderColor" string)


empty : Floor
empty =
    Floor []


fromGrid : List (List Int) -> List Point
fromGrid grid =
    List.indexedMap generateRow grid
        |> List.concat
        |> List.filterMap
            (\f ->
                case f of
                    None ->
                        Nothing

                    Field p ->
                        Just p
            )


generateRow : Int -> List Int -> List Field
generateRow rowIdx row =
    List.indexedMap (generateField rowIdx) row


generateField : Int -> Int -> Int -> Field
generateField rowIdx colIdx field =
    case field of
        0 ->
            None

        1 ->
            Field (Point rowIdx colIdx)

        _ ->
            Field (Point rowIdx colIdx)


sumPoints : Floor -> List Int
sumPoints (Floor points) =
    List.map (\{ x, y } -> x + y) points


floorStart : Floor -> Int
floorStart f =
    sumPoints f |> List.minimum |> Maybe.withDefault 0


floorEnd : Floor -> Int
floorEnd f =
    sumPoints f |> List.maximum |> Maybe.withDefault 0


width : Int -> Floor -> Int
width fs f =
    (floorEnd f - floorStart f + 2) * fs // 2
