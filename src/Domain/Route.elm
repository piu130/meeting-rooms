module Domain.Route exposing (Route(..), fromUrl)

import Domain.Lobby exposing (Lobby)
import Domain.Room exposing (RoomId, idUrlParser)
import Url exposing (Url)
import Url.Parser as Parser exposing ((</>), Parser, oneOf, parse, s, top)


type Route
    = NotFound
    | Lobby
    | Room RoomId


parser : Parser (Route -> a) a
parser =
    oneOf
        [ Parser.map Lobby (oneOf [ top, s "lobby" ])
        , Parser.map Room (s "room" </> idUrlParser)
        ]


fromUrl : Url -> Route
fromUrl url =
    Maybe.withDefault NotFound (parse parser url)
