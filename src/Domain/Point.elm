module Domain.Point exposing (Point, intersperse)


type alias Point =
    { x : Int
    , y : Int
    }


intersperse : String -> Point -> String
intersperse sep { x, y } =
    String.fromInt x ++ sep ++ String.fromInt y
