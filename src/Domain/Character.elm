module Domain.Character exposing (Character, equals)

import Domain.Point exposing (Point)



-- MODEL


type CharacterId
    = CharacterId Int


type Character
    = Character
        { id : CharacterId
        , name : String
        , position : Point
        }



-- VIEW
-- UPDATE


equals : Character -> Character -> Bool
equals (Character c1) (Character c2) =
    c1.id == c2.id
