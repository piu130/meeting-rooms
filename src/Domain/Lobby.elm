module Domain.Lobby exposing (Lobby)

import Domain.Room exposing (Room, RoomId)
import Html exposing (Html, div)



-- MODEL


type Lobby
    = Lobby
        { rooms : List Room
        , selectedRoom : RoomId
        }



-- VIEW


view : Lobby -> Html msg
view (Lobby { rooms }) =
    div [] []



-- UPDATE


select : RoomId -> Lobby -> Lobby
select id (Lobby l) =
    Lobby { l | selectedRoom = id }
