module Domain.Room exposing (Msg(..), Room, RoomId, default, defaultId, defaultNameStyle, defaultStyle, idEncoder, idUrlParser, view, width)

import Color exposing (Color, toCssString)
import Domain.Character as Character exposing (Character)
import Domain.Floor as Floor exposing (Floor, Style)
import Domain.Point as Point exposing (Point)
import Html
import Json.Decode as Decode exposing (Decoder)
import Svg exposing (Svg)
import Svg.Attributes as SvgA
import Url.Parser



-- MODEL


type RoomId
    = RoomId Int


defaultId : RoomId
defaultId =
    RoomId 1234


type Room
    = Room
        { id : RoomId
        , name : String
        , characterLimit : Int
        , characters : List Character
        , floor : Floor
        }


default : Room
default =
    Room
        { id = defaultId
        , name = "abd"
        , characterLimit = 10
        , characters = []
        , floor = Floor.default
        }


type NameStyle
    = NameStyle { color : Color }


defaultNameStyle : NameStyle
defaultNameStyle =
    NameStyle { color = Color.red }


type Style
    = RoomStyle
        { fieldSize : Int
        , fillColor : Color
        , floorStyle : Floor.Style
        , roomNameStyle : NameStyle
        }


defaultStyle : Style
defaultStyle =
    RoomStyle
        { fieldSize = 68
        , fillColor = Color.green
        , floorStyle = Floor.defaultStyle
        , roomNameStyle = defaultNameStyle
        }



-- VIEW


view : Point -> Style -> Room -> Svg Msg
view p ((RoomStyle { roomNameStyle }) as rs) ((Room { name }) as r) =
    Svg.g
        []
        [ viewBackground { x = width rs r, y = 350 } rs
        , viewRoomName p roomNameStyle name
        , viewElements rs r
        ]


viewBackground : Point -> Style -> Svg msg
viewBackground { x, y } (RoomStyle rs) =
    Svg.rect
        [ SvgA.x "0"
        , SvgA.y "0"
        , SvgA.class "room-background"
        , SvgA.width <| String.fromInt x
        , SvgA.height <| String.fromInt y
        , SvgA.stroke "#0000ff"
        , SvgA.fill <| toCssString rs.fillColor
        ]
        []


viewRoomName : Point -> NameStyle -> String -> Svg msg
viewRoomName { x } (NameStyle { color }) name =
    Svg.text_
        [ SvgA.textAnchor "middle"
        , SvgA.fill <| toCssString color
        , SvgA.class "room-name"
        , SvgA.x (String.fromInt (x // 2))
        , SvgA.y "30"
        , SvgA.fontSize "30"
        , SvgA.pointerEvents "none"
        ]
        [ Svg.text name ]


viewElements : Style -> Room -> Svg Msg
viewElements (RoomStyle { fieldSize, floorStyle }) (Room { floor }) =
    let
        translate =
            { x = 10, y = 150 }
    in
    Svg.g
        [ SvgA.class "room-components"
        , SvgA.transform ("translate(" ++ Point.intersperse " " translate ++ ")")
        ]
        [ Html.map FloorMsg (Floor.view fieldSize floorStyle floor) ]


width : Style -> Room -> Int
width (RoomStyle { fieldSize }) (Room { floor }) =
    Floor.width fieldSize floor + 20



-- UPDATE


type Msg
    = FloorMsg Floor.Msg


join : Character -> Room -> Room
join c (Room r) =
    if List.length r.characters <= r.characterLimit then
        Room r

    else
        Room { r | characters = c :: r.characters }


leave : Character -> Room -> Room
leave c (Room r) =
    Room { r | characters = List.filter (Character.equals c >> not) r.characters }



-- TRANSFORM


idDecoder : Decoder RoomId
idDecoder =
    Decode.map RoomId Decode.int


idEncoder : RoomId -> String
idEncoder (RoomId id) =
    String.fromInt id


idUrlParser : Url.Parser.Parser (RoomId -> a) a
idUrlParser =
    Url.Parser.map RoomId Url.Parser.int
