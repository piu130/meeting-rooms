module Api exposing (..)
import Domain.Room exposing (RoomId)
import Domain.Room exposing (idEncoder)
import Http

host: String
host = "http://localhost:3000/"

type alias HttpResult a = Result Http.Error a

fetchRoom : RoomId -> ((HttpResult String) -> msg) -> Cmd msg
fetchRoom id toMsg =
    Http.get
        { url = "http://localhost:3000/rooms/" ++ (idEncoder id)
        , expect = Http.expectString toMsg
        }
