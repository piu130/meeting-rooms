module Main exposing (..)

import Browser
import Browser.Navigation as Nav
import Domain.Point as Point
import Domain.Room as Room
import Domain.Route as Route exposing (Route(..))
import Html exposing (button, div, text)
import Html.Attributes exposing (href)
import Html.Events exposing (onClick)
import Http
import Svg exposing (svg)
import Svg.Attributes as SvgA
import Url exposing (Url)


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }


fetchRoom : String -> Cmd Msg
fetchRoom id =
    Http.get
        { url = "http://localhost:3000/rooms/" ++ id
        , expect = Http.expectString GotRoom
        }


init : () -> Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url navKey =
    ( { counter = 0
      , room = ""
      , navKey = navKey
      , route = Route.fromUrl url
      }
    , fetchRoom "abcde"
    )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


type Msg
    = Increment
    | Decrement
    | RoomMsg Room.Msg
    | GotRoom (Result Http.Error String)
    | LinkClicked Browser.UrlRequest
    | UrlChanged Url
    | GoBack Int


type alias Model =
    { counter : Int
    , room : String
    , route : Route
    , navKey : Nav.Key
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ({ counter, navKey } as model) =
    case msg of
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.navKey (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            ( { model | route = Route.fromUrl url }, Cmd.none )

        GoBack _ ->
            ( model, Nav.back navKey 1 )

        Increment ->
            ( { model | counter = counter + 1 }, Cmd.none )

        Decrement ->
            ( { model | counter = counter - 1 }, Cmd.none )

        GotRoom r ->
            case r of
                Ok room ->
                    ( { model | room = room }, Cmd.none )

                Err _ ->
                    ( model, Cmd.none )

        _ ->
            ( model, Cmd.none )


view : Model -> Browser.Document Msg
view ({ counter, route } as model) =
    let
        room =
            Room.default

        roomWidth =
            Room.width Room.defaultStyle room

        roomBottomRight =
            { x = roomWidth, y = 250 }
    in
    { title = "Bla"
    , body =
        [ div []
            [ button [ onClick Decrement ] [ text "-" ]
            , div [] [ text (String.fromInt counter) ]
            , button [ onClick Increment ] [ text "+" ]
            , div [] [ text model.room ]
            , svg
                [ SvgA.width <| String.fromInt roomWidth
                , SvgA.height "250"
                , SvgA.viewBox <| "0 0 " ++ Point.intersperse " " roomBottomRight
                ]
                [ Html.map RoomMsg (Room.view roomBottomRight Room.defaultStyle room)
                ]
            ]
        , case route of
            NotFound ->
                div []
                    [ text "Not Found"
                    , button [ onClick (GoBack 1) ] [ text "back" ]
                    ]

            _ ->
                div []
                    [ text "success"
                    , button [ href "/lobby1" ] [ text "lobby" ]
                    ]
        ]
    }
